<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'voxloud' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'E-vG_4D&G~lQ_{2I3~2uPEL;c-w>nc@Y*z_1o@G(Ti`%CTr3Ma5Z%};+fm^Zwo%*' );
define( 'SECURE_AUTH_KEY',  '. z^>M{<nR<~b_Q>aH;ZQ-AO0Z`g=f:/nTh|CQNNHWp.sK6[]v]k85xm,X]s2$(!' );
define( 'LOGGED_IN_KEY',    'LiJllHl{$9+n>>IVl%P-Xb4KRl_TdNYP3^O]A,e|^ caHV)n50%3CK?OB%N;ta_%' );
define( 'NONCE_KEY',        'oXb;*b77x]= mLPvF~#,l7kOB bFTt=U0UXxmnYOyFDZ(&NpDGN?^nj+q*YZD11P' );
define( 'AUTH_SALT',        '7Z%i|Kqo_l^`/4j_h;Z>pes-=S#]S@&h2X>oPT.[-gYcn*5F,FtB~<U<7mfB6thW' );
define( 'SECURE_AUTH_SALT', 'Rj_r!kV3`e9(~W&u]L`@s3;amAmo6G:x-k>M,pR:VPjs=,V@rEB_5NS^:^):jz`3' );
define( 'LOGGED_IN_SALT',   'EuB?fhvN2p}<o+m|nKqGWy`owq-m_WN!.`Zylk%1?@vX)Q9,$Fo0]:iI_p*/pM,s' );
define( 'NONCE_SALT',       '09kpGT?>bI1]YwJ &w(|E,R/%RQ7xiltYA+?T{l1[z(Fh3E{,nyV~1Sounz)Lc)k' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'da2_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
