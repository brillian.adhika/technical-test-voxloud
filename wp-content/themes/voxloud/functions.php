<?php
function voxloud_setup()
{
    add_theme_support('automatic-feed-links');
    add_theme_support('html5', array(
        'search-form',
        'comment-form',
        'comment-list'
    ));
    register_nav_menu('primary', __('Navigation Menu', 'voxloud'));
    register_nav_menu('footer', __('Footer Menu', 'voxloud'));

    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(604, 270, true);
    add_filter('use_default_gallery_style', '__return_false');
}
add_action('after_setup_theme', 'voxloud_setup');
add_action('init', 'page_excerpt_init');
function page_excerpt_init() {
       add_post_type_support( 'page', 'excerpt' );
}
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
   add_theme_support( 'woocommerce' );
}
function voxloud_scripts_styles()
{
    if (is_singular() && comments_open() && get_option('thread_comments'))
        wp_enqueue_script('comment-reply');
    wp_enqueue_style( 'voxloud-style', get_stylesheet_uri(), array(), '');
    wp_enqueue_style( 'bootstrap-style', get_template_directory_uri() . 'assets/css/bootstrap.min.css' );
    wp_enqueue_style( 'fa-style', get_template_directory_uri() . 'assets/css/style.css' );
    
    wp_enqueue_script( 'jquery-script', get_template_directory_uri() . '/assets/js/jquery-3.6.0.min.js' );
    wp_enqueue_script( 'bootstrap-script', get_template_directory_uri() . '/assets/js/bootstrap.bundle.min.js' );
    
}
add_action('wp_enqueue_scripts', 'voxloud_scripts_styles');
function voxloud_wp_title($title, $sep)
{
    global $paged, $page;
    if (is_feed())
        return $title;
    $title .= get_bloginfo('name');
    $site_description = get_bloginfo('description', 'display');
    if ($site_description && (is_home() || is_front_page()))
        $title = "$title $sep $site_description";
    if ($paged >= 2 || $page >= 2)
        $title = "$title $sep " . sprintf(__('Page %s', 'voxloud'), max($paged, $page));
    return $title;
}
add_filter('wp_title', 'voxloud_wp_title', 10, 2);
