<footer>
    <div class="container">
      <div class="row">
        <div class="col-md-2">
          <img src="assets/images/logo-bookmark.svg" alt="">
        </div>
        <div class="col-md-7">
          <ul class="list-inline">
            <li class="list-inline-item"><a href="#">Features</a></li>
            <li class="list-inline-item"><a href="#">Pricing</a></li>
            <li class="list-inline-item"><a href="#">Contact</a></li>
          </ul>
        </div>
        <div class="col-md-3">

        </div>
      </div>
    </div>
    <p class="attribution">
      Challenge by <a href="https://www.voxloud.com/en/" target="_blank">Voxloud</a>.
      Coded by <a href="#">Your Name Here</a>.
    </p>
  </footer>
</body>

</html>
<?php wp_footer();?>